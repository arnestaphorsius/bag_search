from django.db import models


class Address(models.Model):
    dm_adres_search_id = models.IntegerField(primary_key=True)
    dm_adres_search_bag = models.BigIntegerField()
    dm_adres_search_postcode = models.CharField(max_length=12)
    dm_adres_search_huisnummer = models.BigIntegerField()
    dm_adres_search_huisletter = models.CharField(max_length=4)
    dm_adres_search_huisnummertoevoeging = models.CharField(max_length=16)
    dm_adres_search_straatnaam = models.CharField(max_length=100)
    dm_adres_search_gemeentenaam = models.CharField(max_length=100)
    dm_adres_search_woonplaats = models.CharField(max_length=100)

    class Meta:
        db_table = 'dm_adres_search'
        ordering = ('dm_adres_search_straatnaam',)


class Street(models.Model):
    dm_adres_search_straatnaam = models.CharField(max_length=100, primary_key=True)
    dm_adres_search_huisnummer = models.BigIntegerField()

    class Meta:
        db_table = 'dm_adres_search'
        ordering = ('dm_adres_search_straatnaam',)
