from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from search import views

urlpatterns = [
    url(r'^straat/$',
        views.StreetList.as_view(),
        name='all-streets'),
    url(r'^straat/(?P<straatnaam>[0-9a-zA-Z -]+)/$',
        views.StreetNameSuggestions.as_view(),
        name='street-suggestions'),
    url(r'^straat/(?P<straatnaam>[0-9a-zA-Z -]+)/nummer/(?P<huisnummer>[0-9]+)/',
        views.AddressSpecifics.as_view(),
        name='address-specifics'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
