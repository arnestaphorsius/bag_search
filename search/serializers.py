from rest_framework import serializers
from search.models import Address


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('dm_adres_search_straatnaam', 'dm_adres_search_huisnummer', 'dm_adres_search_huisnummertoevoeging',
                  'dm_adres_search_postcode', 'dm_adres_search_gemeentenaam', 'dm_adres_search_bag')


class StreetSerializer(serializers.Serializer):
    street = serializers.CharField(max_length=100)
    numbers = serializers.ListField()
