from itertools import groupby
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from search.models import Address
from search.serializers import AddressSerializer, StreetSerializer


class PrettyJSONRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        return super(PrettyJSONRenderer, self).render(data, accepted_media_type, renderer_context={'indent': 4})


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'streets': reverse('all-streets', request=request, format=format),
    })


class StreetList(APIView):
    renderer_classes = (PrettyJSONRenderer, BrowsableAPIRenderer, )

    def get(self, request, format=None):
        return Response({
            'result': Address.objects.all().values_list('dm_adres_search_straatnaam', flat=True).distinct()
        })


class StreetNameSuggestions(APIView):
    """
    This endpoint presents suggestions for streetnames.

    Streetnames are looked up case insensitive and ordered alphabetically.
    """
    renderer_classes = (PrettyJSONRenderer, BrowsableAPIRenderer, )

    def get(self, request, straatnaam, format=None):
        streets = Address.objects.filter(dm_adres_search_straatnaam__istartswith=straatnaam) \
            .values_list('dm_adres_search_straatnaam', flat=True) \
            .distinct()[:10]

        return Response({'result': streets})


class StreetAndHouseNumberSuggestions(generics.ListAPIView):
    """
    This endpoint presents suggestions for streetnames with corresponding house numbers found in the database.

    Streetnames are looked up case insensitive and ordered alphabetically.
    """
    renderer_classes = (PrettyJSONRenderer, BrowsableAPIRenderer, )
    serializer_class = StreetSerializer

    def get_queryset(self, format=None):

        results = list()
        streets = Address.objects.filter(dm_adres_search_straatnaam__istartswith=self.kwargs['straatnaam']) \
                                 .values_list('dm_adres_search_straatnaam', 'dm_adres_search_huisnummer')

        for key, group in groupby(streets, lambda x: x[0]):
            results.append({'street': key, 'numbers': sorted(list(item[1] for item in group))})

        return results


class AddressSpecifics(generics.ListAPIView):
    """
    This endpoint returns the postcode, region and BAG identifier for a given address.
    """
    renderer_classes = (PrettyJSONRenderer, BrowsableAPIRenderer, )
    serializer_class = AddressSerializer

    def get_queryset(self):
        straatnaam = self.kwargs.get('straatnaam', None)
        huisnummer = self.kwargs.get('huisnummer', None)

        return Address.objects.filter(dm_adres_search_straatnaam__iexact=straatnaam)\
                              .filter(dm_adres_search_huisnummer=huisnummer)
